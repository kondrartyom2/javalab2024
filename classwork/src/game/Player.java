package game;

public class Player extends Object{
    private int hp;
    private String name;

    public Player(String name) {
        this.hp = 100;
        this.name = name;
    }

    public int getHp() {
        return hp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void minusHp(int power) {
        this.hp -= power * 10;
    }

    public boolean isAlive() {
        return this.hp > 0;
    }

    @Override
    public String toString() {
        return "Player{\n hp=%s, \nname=%s\n}".formatted(hp, name);
    }
}
